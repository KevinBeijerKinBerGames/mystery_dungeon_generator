﻿using System;

/// <summary>
/// Public enumerator of all the different tile types that exist.
/// </summary>
public enum TileTypes
{
    Unvisited = -1,     // All tiles start out as Unvisited and will be "filled" when they get visited.
    Floor = 0,          // Simply the floor.
    Wall = 1,           // The walls of the dungeon.
}   

