﻿using System;

/// <summary>
/// Class that defines the position of one tile of the Dungeon.
/// </summary>
public class Position
{
    int row = 0;    // Row of the tile.
    int col = 0;    // Column of the tile.

    /// <summary>
    /// Able to set and get the Row position of a tile.
    /// </summary>
    public int Row
    {
        get { return row; }
        set { row = value; }
    }

    /// <summary>
    /// Able to set and get the Column position of a tile.
    /// </summary>
    public int Col
    {
        get { return col; }
        set { col = value; }
    }

    /// <summary>
    /// To set row and col value.
    /// </summary>
    /// <param name="c">Amount of columns.</param>
    /// <param name="r">Amount of rows.</param>
    public Position(int c, int r)
    {
        row = r;
        col = c;
    }
}
