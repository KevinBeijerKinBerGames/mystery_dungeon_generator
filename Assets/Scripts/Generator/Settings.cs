﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Settings for the generator.
/// </summary>
[CreateAssetMenu]
public class Settings : ScriptableObject
{
    /// <summary>
    /// Size of the playing field.
    /// </summary>
    [Range (50, 100)]
    public int playSize = 50;

    /// <summary>
    /// Amount of rooms on the field.
    /// </summary>
    [Range (3, 10)]
    public int roomAmount = 3;

    /// <summary>
    /// Minimum room size.
    /// </summary>
    [Range (3, 5)]
    public int minRoomSize = 3;

    /// <summary>
    /// Maximum room size.
    /// </summary>
    [Range (5, 20)]
    public int maxRoomSize = 10;
}
