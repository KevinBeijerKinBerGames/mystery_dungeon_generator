﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dungeon Generator.
/// Generates the "Mystery" Dungeon.
/// </summary>
public class DungeonGenerator : MonoBehaviour
{
    // Starting panel, will be set inactive when playing.
    public GameObject start;

    // Prefabs for floor and wall.
    public GameObject floorPrefab;
    public GameObject wallPrefab;

    // Prefab for the player character.
    public GameObject player;
    GameObject user;
    PlayerController playerScript;

    // Settings object (which settings to use).
    public Settings settings;

    // If the dungeon is Done or not.
    public bool Done { get; private set; }

    // Width and height of the map.
    private int width;
    private int height;

    // creates a Tiletypes Array
    TileTypes[,] dungeon;

    // Current Position
    Position current;

    // Stack of open Positions.
    Stack<Position> openPositions;

    /// <summary>
    /// Start generation of the dungeon.
    /// </summary>
    void Start()
    {
        start.SetActive(true);
        openPositions = new Stack<Position>(width * height);
        Done = false;

        GenerateDungeon();
        DrawDungeon();

        playerScript = user.GetComponent<PlayerController>();
        playerScript.enabled = false;
    }

    /// <summary>
    /// Key commands for the generator.
    /// </summary>
    private void Update()
    {
        // Tab - Destroys everything on screen and starts anew.
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            DestroyImmediate(user);
            ClearDungeon();
            Start();
        }
        /*
        // If you want to manually generate, un-comment this.
        // Space - Next generation step.
        if (Input.GetKey(KeyCode.Space))
        {
            GenerationStep();
            ClearDungeon();
            DrawDungeon();
        }
        */
        // Return - Finish the process of generating.
        if (Input.GetKeyDown(KeyCode.Return))
        {
            playerScript.enabled = true;
            start.SetActive(false);
            while (!Done)
            {
                GenerationStep();
            }
            ClearDungeon();
            DrawDungeon();
        }
    }

    /// <summary>
    /// Clears the Dungeon so no duplicates will be made.
    /// </summary>
    void ClearDungeon()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Generate the next step.
    /// </summary>
    void GenerationStep()
    {
        if (Done)
        {
            return;
        }

        // Get random options.
        var options = GetOptions(current);

        // No options = go back.
        if (options.Count == 0)
        {
            // No more options left = finish dungeon.
            if (openPositions.Count == 0)
            {
                FinishDungeon();
            }
            else
            {
                current = openPositions.Pop();
            }
            return;
        }

        // Pick random option.
        var randomOption = options[Random.Range(0, options.Count)];

        // Add floor to option location.
        dungeon[randomOption.Col, randomOption.Row] = TileTypes.Floor;

        // add walls
        AddWalls(current, current.Col == randomOption.Col);

        options = GetOptions(current);

        // more options, safe it for later.
        if (options.Count > 0)
        {
            openPositions.Push(current);
        }

        // set new position as current
        current = randomOption;
    }

    /// <summary>
    /// Finishes the Dungeon by filling in unreachable places with walls.
    /// </summary>
    void FinishDungeon()
    {
        Done = true;

        if (Done == true)
        {
            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    if (dungeon[col, row] == TileTypes.Unvisited)
                    {
                        dungeon[col, row] = TileTypes.Wall;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Add the walls to the current position.
    /// </summary>
    /// <param name="position">Takes the position which is being used.</param>
    /// <param name="goingVertical">If the movement went Vertical (if not it will be horizontal)</param>
    void AddWalls(Position position, bool goingVertical)
    {
        if (goingVertical)
        {
            if (dungeon[position.Col - 1, position.Row] == TileTypes.Unvisited)
            {
                dungeon[position.Col - 1, position.Row] = TileTypes.Wall;
            }
            if (dungeon[position.Col + 1, position.Row] == TileTypes.Unvisited)
            {
                dungeon[position.Col + 1, position.Row] = TileTypes.Wall;
            }
        }
        else
        {
            if (dungeon[position.Col, position.Row - 1] == TileTypes.Unvisited)
            {
                dungeon[position.Col, position.Row - 1] = TileTypes.Wall;
            }
            if (dungeon[position.Col, position.Row + 1] == TileTypes.Unvisited)
            {
                dungeon[position.Col, position.Row + 1] = TileTypes.Wall;
            }
        }
    }

    /// <summary>
    /// List of possible options that can be taken from a certain tile.
    /// </summary>
    /// <param name="current">Current position.</param>
    /// <returns>Returns all possible options.</returns>
    List<Position> GetOptions(Position current)
    {
        var result = new List<Position>();
        if (dungeon[current.Col, current.Row + 1] == TileTypes.Unvisited)
        {
            result.Add(new Position(current.Col, current.Row + 1));
        }
        if (dungeon[current.Col, current.Row - 1] == TileTypes.Unvisited)
        {
            result.Add(new Position(current.Col, current.Row - 1));
        }
        if (dungeon[current.Col + 1, current.Row] == TileTypes.Unvisited)
        {
            result.Add(new Position(current.Col + 1, current.Row));
        }
        if (dungeon[current.Col - 1, current.Row] == TileTypes.Unvisited)
        {
            result.Add(new Position(current.Col - 1, current.Row));
        }
        return result;
    }

    /// <summary>
    /// Reads the information for every tile and places the prefabs in the right places.
    /// </summary>
    void DrawDungeon()
    {
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                if (dungeon[col, row] == TileTypes.Wall)
                {
                    CreateTile(wallPrefab, col, row);
                }
                else if (dungeon[col, row] == TileTypes.Floor)
                {
                    CreateTile(floorPrefab, col, row);
                }
            }
        }
    }

    /// <summary>
    /// Creates the Prefab to be placed.
    /// </summary>
    /// <param name="prefab">Prefab to be instantiated.</param>
    /// <param name="col">Column where the prefab needs to be instantiated.</param>
    /// <param name="row">Row where the prefab needs to be instantiated.</param>
    void CreateTile(GameObject prefab, int col, int row)
    {
        var go = Instantiate(prefab, transform);
        go.transform.localPosition = new Vector3(col, prefab.transform.position.y, row);
    }

    /// <summary>
    /// Generates the first basic dungeon.
    /// Creates the wall outline, makes everything unvisited inside, and creates set amount of rooms.
    /// </summary>
    void GenerateDungeon()
    {
        width = settings.playSize;
        height = settings.playSize;

        dungeon = new TileTypes[width, height];

        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                if (row == 0 || row == height - 1 || col == 0 || col == width - 1)
                    dungeon[col, row] = TileTypes.Wall;
                else
                    dungeon[col, row] = TileTypes.Unvisited;
            }
        }

        // For all rooms that are in roomAmount.
        for (int i = 0; i < settings.roomAmount; i++)
        {
            // The to be used size of the room
            int roomSize = Random.Range(settings.minRoomSize, settings.maxRoomSize);

            // Create random Column and Row value.
            int randomCol = Random.Range(1, width - roomSize - 2);
            int randomRow = Random.Range(1, height - roomSize - 2);
                        
            // Set current position with both random values.
            current = new Position(randomCol, randomRow);

            // Start position for the player. (middle of the room) And makes sure the player is only spawned in the first room.
            Vector3 startPosition = new Vector3(current.Col + roomSize / 2 + 1, 0, current.Row + roomSize / 2 + 1);
            if (i == 0)
            {
                user = Instantiate(player, startPosition, Quaternion.identity);
            }

            // Creates the room to size.
            for (int x = 0; x < roomSize + 2; x++)
            {
                for (int z = 0; z < roomSize + 2; z++)
                {
                    if (x == 0 || x == roomSize + 1 || z == 0 || z == roomSize + 1)
                    {
                        if (x == roomSize / 2 + 1 || z == roomSize / 2 + 1)
                        {
                            dungeon[current.Col + x, current.Row + z] = TileTypes.Floor;

                            Position exit = new Position(current.Col + x, current.Row + z);
                            openPositions.Push(exit);
                        }
                        else
                        {
                            dungeon[current.Col + x, current.Row + z] = TileTypes.Wall;
                        }
                    }
                    else
                    {
                        dungeon[current.Col + x, current.Row + z] = TileTypes.Floor;
                    }
                }
            }
            // Resets current to one of the openings of the room.
            current = new Position(randomCol + roomSize/2, randomRow + roomSize/2);
        }
    }
}
