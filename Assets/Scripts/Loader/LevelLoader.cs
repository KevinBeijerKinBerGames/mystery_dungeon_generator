﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Loads a certain scene with a loading bar.
/// </summary>
public class LevelLoader : MonoBehaviour
{
    public GameObject loadingScreen;    // UI Panel to be active or inactive.
    public Slider slider;               // Slider for the loading bar.
    public GameObject button;           // Button to be able to be active or inactive.
    public Text progressText;           // Text for the loading progress.

    /// <summary>
    /// Start the loading coroutine.
    /// </summary>
    /// <param name="sceneIndex">Index of the to be loaded scene.</param>
    public void LoadLevel (int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
        button.SetActive(false);
    }

    /// <summary>
    /// Coroutine to load a scene with a loading bar.
    /// </summary>
    /// <param name="sceneIndex">The to be loaded scene.</param>
    IEnumerator LoadAsynchronously (int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            slider.value = progress;
            progressText.text = progress * 100f + " %";

            yield return null;
        }
    }
}
