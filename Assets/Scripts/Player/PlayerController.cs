﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple controller for the player.
/// </summary>
public class PlayerController : MonoBehaviour
{
    public Vector3 attempt;

    /// <summary>
    /// Move commands for the player.
    ///  W or up arrow to go up, S or down go down, A or left arrow to rotate 90 degrees to the left, D or right arrow to rotate to the right.
    /// </summary>
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            attempt = new Vector3(0, 0, 1);
            transform.Translate(attempt, Space.Self);
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -90);
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            attempt = new Vector3(0, 0, -1);
            transform.Translate(attempt, Space.Self);
        }
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, 90);
        }
    }

    /// <summary>
    /// In case of running into a wall, put the player back at previous position.
    /// </summary>
    /// <param name="other">Other collider (probably wall)</param>
    private void OnTriggerStay(Collider other)
    {
        transform.Translate(-attempt/3);
    }
}
