Welcome to this dungeon generator. 

This works fairly simply so no worries.
When you run the build, you are in a 3d maze with a couple of rooms.
You can walk forwards and backwards with W or the up arrow and S or the down arrow.
You can turn 90 degrees on the spot with A (for left) and D (for right).

In the dungeon generator code, in the update function, there 
is a block that is commented out. If you uncomment that block, 
and comment block before it, you can manually generating the 
dungeon by holding SPACE (for tile by tile) or pressing RETURN 
to finish the dungeon. (Generating manually drops the framerate
and is turned off by default for that reason)

You can also change the settings. In the settings folder there is a testing settings.
You can change the several variables there to change what impact they have on the 
generation. All variables kinda speak for themselves, but if not, open the settings script.

Shoutouts to Koji Kondo for making the The Legend of Zelda: Ocarina of Time soundtracks. They're great.